import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

let API_ENDPOINT = 'http://178.62.10.209:60080/api/v1';

Vue.use(Vuex);
export default new Vuex.Store({
    modules: {
        auth: {
            namespaced: true,
            state: function () {
                return {
                    user: {
                        id: 0,
                        username: '',
                        email: ''
                    },
                    token: localStorage.getItem('token') || '',
                    is_error: false,
                    error_data: {}
                }
            },
            mutations: {
                save(state, {type, payload}) {
                    state[type] = payload
                },
                update_token(state, {token}) {
                    state['token'] = token;
                    localStorage.setItem('token', token);
                },
                clear(state) {
                    state['user'] = {};
                    state['token'] = '';
                    localStorage.removeItem('token');
                }
            },
            actions: {
                login: function (context, data) {
                    let username = data.username, password = data.password;

                    axios.post(API_ENDPOINT + '/auth/login/', {username: username, password: password})
                        .then(response => {
                            let token = response.data.key;
                            context.commit('update_token', {
                                token: token
                            });

                            context.commit('save', {
                                type: 'is_error',
                                payload: false
                            });

                            axios.get(API_ENDPOINT + '/auth/user/', {
                                headers: {
                                    'Authorization': 'Token ' + token
                                }
                            })
                                .then(response => {
                                    context.commit('save', {
                                        type: 'user',
                                        payload: response.data
                                    });
                                });

                        })
                        .catch(error => {
                            context.commit('save', {
                                type: 'is_error',
                                payload: true
                            });

                            context.commit('save', {
                                type: 'error_data',
                                payload: error
                            });

                        })

                },
                logout: function (context) {
                    return new Promise((resolve, reject) => {
                        axios.post('http://178.62.10.209:60080/api/v1/auth/logout/', {}, {
                            headers: {
                                'Authorization': 'Token ' + context.rootState.auth.token
                            }
                        }).then(response => {
                            context.commit('clear');
                            resolve();
                        }).catch(error => {
                            console.log(error);
                            reject();
                        });
                    });
                }
            }
        },
        posts: {
            namespaced: true,
            state: function () {
                return {
                    list: []
                }
            },
            mutations: {
                save(state, {type, payload}) {
                    state[type] = payload
                }
            },
            actions: {
                get_posts: function (context, {limit, offset}) {
                    axios.get(API_ENDPOINT + '/blogs/?limit=' + limit + '&offset=' + offset , {
                        headers: {
                            'Authorization': 'Token ' + context.rootState.auth.token
                        }
                    }).then(response => {
                        context.commit('save', {
                            type: 'list',
                            payload: response.data
                        });
                    });
                }
            }
        },
        post: {
            namespaced: true,
            state: function () {
                return {
                    data: {
                        id: 0,
                        title: '',
                        short: '',
                        content: ''
                    }
                }
            },
            mutations: {
                save(state, {type, payload}) {
                    state[type] = payload
                }
            },
            actions: {
                get_post: function (context, id) {
                    axios.get(API_ENDPOINT + '/blogs/' + id, {
                        headers: {
                            'Authorization': 'Token ' + context.rootState.auth.token
                        }
                    }).then(response => {
                        context.commit('save', {
                            type: 'data',
                            payload: response.data
                        });
                    });
                }
            }
        },
    },
})
