import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
//

Vue.use(Router);
export const HTTP = axios.create({
  baseURL: `http://jsonplaceholder.typicode.com/`,
  headers: {
    Authorization: 'Bearer {token}'
  }

});
// const store = new Vuex.Store({
//   // ...
//
// });
export default new Router({
  routes: [
    { path: '/', name: 'Login', component: () =>import('@/components/Login') },
    { path: '/Exit', name: 'Exit', component: () =>import('@/components/Exit') },
    { path: '/SignUp', name: 'SignUp', component: () => import('@/components/SignUp')},
    { path: '/Blog/:page', name: 'Blog', props: true, component: () => import('@/components/Blog') },
    { path: '/Post/:post_id', name: 'Post', component: () => import('@/components/Post')},
    { path: '/NewPost', name: 'NewPost', component: () => import('@/components/NewPost')},
    { path: '/Post/:post_id/edit', name: 'EditPost', component: () => import('@/components/EditPost')}
  ]
})
